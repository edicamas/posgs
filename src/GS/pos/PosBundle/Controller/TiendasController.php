<?php

namespace GS\pos\PosBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use GS\pos\PosBundle\Entity\Tiendas;
use GS\pos\PosBundle\Form\TiendasType;

/**
 * Tiendas controller.
 *
 * @Route("/tiendas")
 */
class TiendasController extends Controller
{

    /**
     * Lists all Tiendas entities.
     *
     * @Route("/", name="tiendas")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('PosBundle:Tiendas')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Tiendas entity.
     *
     * @Route("/", name="tiendas_create")
     * @Method("POST")
     * @Template("PosBundle:Tiendas:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Tiendas();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            $idCadena = $entity->getIdCadena()->getId();
            return $response = new JsonResponse($idCadena);
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Tiendas entity.
     *
     * @param Tiendas $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Tiendas $entity)
    {
        $form = $this->createForm(new TiendasType(), $entity, array(
            'action' => $this->generateUrl('tiendas_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Tiendas entity.
     *
     * @Route("/new/{idCadena}", name="tiendas_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction($idCadena)
    {
        $em = $this->getDoctrine()->getManager();
        $cadena = $em->getRepository('PosBundle:Cadenas')->find($idCadena);
        $entity = new Tiendas();
        $entity->setIdCadena($cadena);
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Tiendas entity.
     *
     * @Route("/{id}", name="tiendas_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PosBundle:Tiendas')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Tiendas entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Tiendas entity.
     *
     * @Route("/{id}/edit", name="tiendas_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PosBundle:Tiendas')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Tiendas entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Tiendas entity.
    *
    * @param Tiendas $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Tiendas $entity)
    {
        $form = $this->createForm(new TiendasType(), $entity, array(
            'action' => $this->generateUrl('tiendas_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Tiendas entity.
     *
     * @Route("/{id}", name="tiendas_update")
     * @Method("POST")
     * @Template("PosBundle:Tiendas:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PosBundle:Tiendas')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Tiendas entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);
        if ($editForm->isValid()) {
            $em->flush();

            $idCadena = $entity->getIdCadena()->getId();
            return $response = new JsonResponse($idCadena);
        }
        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Tiendas entity.
     *
     * @Route("/{id}", name="tiendas_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('PosBundle:Tiendas')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Tiendas entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('tiendas'));
    }

    /**
     * Creates a form to delete a Tiendas entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('tiendas_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }

    public function selectTiendaAction($idCadena)
    {
        $em = $this->getDoctrine()->getManager();
        $tiendas = $em->getRepository('PosBundle:Tiendas')->findBy(array(
            'idCadena' => '$idCadena'
        ));

        return $this->render("PosBundle:Tiendas:optiontiendas.html.twig",array(
            'tiendas' => $tiendas
        ));
    }
    public function listTiendasAction($idCadena){
        $em = $this->getDoctrine()->getManager();
        $listadoTiendas = $em->getRepository('PosBundle:Tiendas')->findBy(array(
            'idCadena' => $idCadena
        ));
        return $this->render('PosBundle:Tiendas:listadoTiendas.html.twig',array(
            'tiendas' => $listadoTiendas,
            'idCadena' => $idCadena
        ));
    }
}
