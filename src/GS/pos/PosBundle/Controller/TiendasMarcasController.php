<?php

namespace GS\pos\PosBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use GS\pos\PosBundle\Entity\TiendasMarcas;
use GS\pos\PosBundle\Form\TiendasMarcasType;

/**
 * TiendasMarcas controller.
 *
 * @Route("/tiendasmarcas")
 */
class TiendasMarcasController extends Controller
{

    /**
     * Lists all TiendasMarcas entities.
     *
     * @Route("/", name="tiendasmarcas")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('PosBundle:TiendasMarcas')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new TiendasMarcas entity.
     *
     * @Route("/", name="tiendasmarcas_create")
     * @Method("POST")
     * @Template("PosBundle:TiendasMarcas:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new TiendasMarcas();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('tiendasmarcas_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a TiendasMarcas entity.
     *
     * @param TiendasMarcas $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(TiendasMarcas $entity)
    {
        $form = $this->createForm(new TiendasMarcasType(), $entity, array(
            'action' => $this->generateUrl('tiendasmarcas_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new TiendasMarcas entity.
     *
     * @Route("/new/{idTienda}", name="tiendasmarcas_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction($idTienda)
    {
        $em = $this->getDoctrine()->getManager();
        $tienda = $em->getRepository('PosBundle:Tiendas')->find($idTienda);
        $entity = new TiendasMarcas();

        $entity->setIdTienda($tienda);
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a TiendasMarcas entity.
     *
     * @Route("/{id}", name="tiendasmarcas_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder();
        $entity = $em->getRepository('PosBundle:TiendasMarcas')->find($id);
        $conteo = $qb->select('count(m.id)')
                     ->from('PosBundle:TiendasMarcas','m')
                     ->where('m.idTienda = :idTienda')
                     ->setParameter('idTienda',$entity->getIdTienda()->getId())
                     ->getQuery()->getSingleScalarResult();

       

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TiendasMarcas entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
            'conteo' => $conteo+1
        );
    }

    /**
     * Displays a form to edit an existing TiendasMarcas entity.
     *
     * @Route("/{id}/edit", name="tiendasmarcas_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PosBundle:TiendasMarcas')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TiendasMarcas entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a TiendasMarcas entity.
    *
    * @param TiendasMarcas $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(TiendasMarcas $entity)
    {
        $form = $this->createForm(new TiendasMarcasType(), $entity, array(
            'action' => $this->generateUrl('tiendasmarcas_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing TiendasMarcas entity.
     *
     * @Route("/{id}", name="tiendasmarcas_update")
     * @Method("PUT")
     * @Template("PosBundle:TiendasMarcas:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PosBundle:TiendasMarcas')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TiendasMarcas entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('tiendasmarcas_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a TiendasMarcas entity.
     *
     * @Route("/{id}", name="tiendasmarcas_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('PosBundle:TiendasMarcas')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TiendasMarcas entity.');
        }

        $em->remove($entity);
        $em->flush();
        return $response = new JsonResponse(1);
    }

    /**
     * Creates a form to delete a TiendasMarcas entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('tiendasmarcas_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
    public function listadoMarcasAction($idTienda){
        $em = $this->getDoctrine()->getManager();
        $marcas = $em->getRepository('PosBundle:TiendasMarcas')->findBy(array(
            'idTienda' => $idTienda
        ));
        return $this->render('PosBundle:TiendasMarcas:listadoMarcas.html.twig',array(
            'marcas' => $marcas
        ));
    }
}
