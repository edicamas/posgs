<?php

namespace GS\pos\PosBundle\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use GS\pos\PosBundle\Entity\Usuarios;
use GS\pos\PosBundle\Form\UsuariosType;

/**
 * Usuarios controller.
 *
 * @Route("/usuarios")
 */
class UsuariosController extends Controller
{

    /**
     * Lists all Usuarios entities.
     *
     * @Route("/", name="usuarios")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('PosBundle:Usuarios')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Usuarios entity.
     *
     * @Route("/", name="usuarios_create")
     * @Method("POST")
     * @Template("PosBundle:Usuarios:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Usuarios();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('usuarios'));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Usuarios entity.
     *
     * @param Usuarios $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Usuarios $entity)
    {
        $form = $this->createForm(new UsuariosType(), $entity, array(
            'action' => $this->generateUrl('usuarios_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array(
            'label' => 'Create',
            'attr' => array(
                        'class'=>'btn btn-success'  
                        )
            ));

        return $form;
    }

    /**
     * Displays a form to create a new Usuarios entity.
     *
     * @Route("/new", name="usuarios_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Usuarios();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Usuarios entity.
     *
     * @Route("/{id}", name="usuarios_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $datos =array();
        $entity = $em->getRepository('PosBundle:Usuarios')->find($id);
        $datos['entity'] = $entity;
        return $datos;
    }

    /**
     * Displays a form to edit an existing Usuarios entity.
     *
     * @Route("/{id}/edit", name="usuarios_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PosBundle:Usuarios')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Usuarios entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Usuarios entity.
    *
    * @param Usuarios $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Usuarios $entity)
    {
        $form = $this->createForm(new UsuariosType(), $entity, array(
            'action' => $this->generateUrl('usuarios_update', array('id' => $entity->getId())),
           
        ));

        $form->add('submit', 'button', array(
            'label' => 'Editar Usuario',
            'attr'=>array(
                        'class'=>'btn btn-success',
                        'style'=>'margin-left:200px',
                        'onclick'=>'submitEdit(this)'
                        )
            ));

        return $form;
    }
    /**
     * Edits an existing Usuarios entity.
     *
     * @Route("/{id}", name="usuarios_update")
     * @Method("PUT")
     * @Template("PosBundle:Usuarios:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PosBundle:Usuarios')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Usuarios entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $response = new response(1);
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Usuarios entity.
     *
     * @Route("/{id}", name="usuarios_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
            
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('PosBundle:Usuarios')->find($id);
            $em->remove($entity);
            $em->flush();
        return $response = new Response(1);
    }

    /**
     * Creates a form to delete a Usuarios entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('usuarios_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }

    public function listAction($rol)
    {
        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder();

        $entities = $qb->Select("u.usuario,u.descripcion,u.id,u.role")
                       ->from("PosBundle:Usuarios",'u');
         switch ($rol) {
                      case 1:
                        $entities = $qb->andwhere("u.role='S'");
                        break;
                      case 2:
                        $entities = $qb->andwhere("u.role='C'");
                        break; 
                      default:
                        $entities = $qb;
                        break;
                        }//Fin de Swtich Rol         
       
        $entities = $qb->getQuery()->getResult();
        return $this->render('PosBundle:Usuarios:usuarioslist.html.twig', array(
            'entities' => $entities,
        ));
    }

    public function tiendasAsignadasAction($idUsuario)
    {
        $em = $this->getDoctrine()->getManager();
        $datos = array(
            'tiendas'     => null,
            'cadenasForm' => null
            );
        $entity = $em->getRepository('PosBundle:Usuarios')->find($idUsuario);
        $datos['entity']=$entity;
        $deleteForm = $this->createDeleteForm($idUsuario);
        if ($entity->getRole() == 'S')
        {
            $qb = $em->createQueryBuilder();
            //Consulta para Obtener las tiendas que tiene Asignadas por supervisor
            $tiendasSupervisor = $qb->select("t.nombreTda,t.direccionTda,st.id")
                                    ->from("PosBundle:SupervisorTienda", 'st')
                                    ->join("PosBundle:Tiendas",'t',\Doctrine\ORM\Query\Expr\Join::WITH,"t.id=st.idTienda")
                                    ->where("st.idUsuario='$idUsuario'")->getQuery()->getResult(); 
            $formCadenas  =  $this  -> getCadenasForm(); 
            $datos['tiendas'] = $tiendasSupervisor;
            $datos['cadenasForm'] = $formCadenas->createView();
        }    
        return $this->render('PosBundle:Usuarios:tiendasasignadas.html.twig',$datos);
    }

    private function getCadenasForm()
    {
        $form = $this->createFormBuilder()
        ->add('cadenas','entity',array(
            'label' => 'Selecciona Cadena:',
            'attr'  => array(
                         'class' => 'form-control'),
            'class' => 'PosBundle:Cadenas'
            ))->getForm();

        return $form;
    }

    public function marcasAsignadasAction($idUsuario)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('PosBundle:Usuarios')->find($idUsuario);
        $qb = $em->createQueryBuilder();
        $marcasClientes = $qb->select("m.descripcion")
                             ->from("PosBundle:Marca","m")
                             ->where("m.idUsuario ='$idUsuario'")->getQuery()->getResult();
        return $this->render('PosBundle:Usuarios:marcasAsignadas.html.twig',array(
            'entity' => $entity,
            'marcas' => $marcasClientes,
            ));
    }
}

 