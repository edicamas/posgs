<?php

namespace GS\pos\PosBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use GS\pos\PosBundle\Entity\SupervisorTienda;
use GS\pos\PosBundle\Form\SupervisorTiendaType;

/**
 * SupervisorTienda controller.
 *
 * @Route("/supervisortienda")
 */
class SupervisorTiendaController extends Controller
{

    /**
     * Lists all SupervisorTienda entities.
     *
     * @Route("/", name="supervisortienda")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('PosBundle:SupervisorTienda')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new SupervisorTienda entity.
     *
     * @Route("/", name="supervisortienda_create")
     * @Method("POST")
     * @Template("PosBundle:SupervisorTienda:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new SupervisorTienda();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('supervisortienda_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a SupervisorTienda entity.
     *
     * @param SupervisorTienda $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(SupervisorTienda $entity)
    {
        $form = $this->createForm(new SupervisorTiendaType(), $entity, array(
            'action' => $this->generateUrl('supervisortienda_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new SupervisorTienda entity.
     *
     * @Route("/new", name="supervisortienda_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new SupervisorTienda();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a SupervisorTienda entity.
     *
     * @Route("/{id}", name="supervisortienda_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PosBundle:SupervisorTienda')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find SupervisorTienda entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing SupervisorTienda entity.
     *
     * @Route("/{id}/edit", name="supervisortienda_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PosBundle:SupervisorTienda')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find SupervisorTienda entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a SupervisorTienda entity.
    *
    * @param SupervisorTienda $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(SupervisorTienda $entity)
    {
        $form = $this->createForm(new SupervisorTiendaType(), $entity, array(
            'action' => $this->generateUrl('supervisortienda_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing SupervisorTienda entity.
     *
     * @Route("/{id}", name="supervisortienda_update")
     * @Method("PUT")
     * @Template("PosBundle:SupervisorTienda:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PosBundle:SupervisorTienda')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find SupervisorTienda entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('supervisortienda_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a SupervisorTienda entity.
     *
     * @Route("/{id}", name="supervisortienda_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('PosBundle:SupervisorTienda')->find($id);
            $em->remove($entity);
            $em->flush();
            return $response = new Response(1);
    }

    /**
     * Creates a form to delete a SupervisorTienda entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('supervisortienda_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }

    public function listTiendasAction($idUser,$idCadena)
    {
        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder();
        $listTiendas = $qb->select("t.id,t.nombreTda,t.direccionTda")
                               ->from("PosBundle:Tiendas",'t')
                               ->leftjoin("PosBundle:SupervisorTienda",'st',\Doctrine\ORM\Query\Expr\Join::WITH,"t.id=st.idTienda and st.idUsuario='$idUser'")
                               ->where("st.idUsuario is null and t.idCadena='$idCadena'")->getQuery()->getResult();
       
        return $this->render("PosBundle:SupervisorTienda:tiendaslist.html.twig",array(
                    'listado'=>$listTiendas,
                    'idUser' => $idUser  
        ));
    }

    public function asignarAction($idUser,$idTienda)
    {
        $em = $this->getDoctrine()->getManager();
        $supervisorTienda = new SupervisorTienda();
        $tienda = $em->getRepository('PosBundle:Tiendas')->find($idTienda);
        $usuario = $em->getRepository('PosBundle:Usuarios')->find($idUser);
        $supervisorTienda->setIdTienda($tienda);
        $supervisorTienda->setIdUsuario($usuario);
        $em->persist($supervisorTienda);
        $em->flush();
        return $response = new Response(1);

    }
}
