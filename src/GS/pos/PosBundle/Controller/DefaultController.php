<?php

namespace GS\pos\PosBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\SecurityContext;
use GS\pos\PosBundle\Entity\Usuarios;


class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('PosBundle:Default:index.html.twig');
    }
    public function loginAction(){
    //$factory = $this->get('security.encoder_factory');
    //$user = new Usuarios();
    //$encoder = $factory->getEncoder($user);
    //$password = $encoder->encodePassword('gspgtwb1', $user->getSalt());
    //$user->setContrasenia($password);
    //echo $user->getContrasenia();
     return $this->render('PosBundle:Default:index.html.twig', array(
       'last_username' => $this->get('request')
                               ->getSession()
                               ->get(SecurityContext::LAST_USERNAME),
       'error'         => $this->get('request')
                               ->getSession()
                               ->get(SecurityContext::AUTHENTICATION_ERROR),
            )
        );
  }
  public function principalAction(){
  	return $this->render('PosBundle:Default:principal.html.twig');
  }
  public function indexSupervisorAction(){
    $monthForm = $this->createMonthForm();
    $monthForm->get('mes')->setData(date('n'));
    return $this->render('PosBundle:Default:indexSupervisor.html.twig',array(
        'porcentajeTiendas' => $this->porcentajeTienda($this->getUser()->getId()),
        'monthForm' => $monthForm->createView()
    ));
  }
  public function createMonthForm(){
    $form = $this->createFormBuilder()
                 ->add('mes','choice',array(
                    'label' => 'Mes',
                    'attr'  => array(
                        'class' => 'form-control'
                    ),
                    'choices' => array(
                      '1' => 'Enero',
                      '2' => 'Febrero',
                      '3' => 'Marzo',
                      '4' => 'Abril',
                      '5' => 'Mayo',
                      '6' => 'Junio',
                      '7' => 'Julio',
                      '8' => 'Agosto',
                      '9' => 'Septiembre',
                      '10' => 'Octubre',
                      '11' => 'Noviembre',
                      '12' => 'Diciembre'
                    )
                 ))->getForm();
        return $form;
  }
  public function porcentajeTienda($idUsuario){
    $em = $this->getDoctrine()->getManager();
    $connection = $em->getConnection();
    $consultaTiendas = $connection->prepare("SELECT (
    (
        SELECT count(distinct(fr.id_tienda))
        FROM flash_report AS fr
        WHERE fr.id_usuario = $idUsuario GROUP BY fr.id_tienda
    )
        /
    (
    SELECT count(st.id)
    FROM supervisor_tienda AS st
    WHERE st.id_usuario = $idUsuario
    )) * 100 porcentaje");
    $consultaTiendas->execute();
    $porcentajeTiendas = $consultaTiendas->fetchAll();
    return $porcentajeTiendas[0]['porcentaje'];
  }
}
