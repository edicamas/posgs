<?php

namespace GS\pos\PosBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use GS\pos\PosBundle\Entity\FlashReport;
use GS\pos\PosBundle\Form\FlashReportType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * FlashReport controller.
 *
 * @Route("/flashreport")
 */
class FlashReportController extends Controller
{

    /**
     * Lists all FlashReport entities.
     *
     * @Route("/", name="flashreport")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('PosBundle:FlashReport')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new FlashReport entity.
     *
     * @Route("/", name="flashreport_create")
     * @Method("POST")
     * @Template("PosBundle:FlashReport:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new FlashReport();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('flashreport_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a FlashReport entity.
     *
     * @param FlashReport $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(FlashReport $entity)
    {
        $form = $this->createForm(new FlashReportType(), $entity, array(
            'action' => $this->generateUrl('flashreport_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array(
            'label' => "Guardar Reporte",
            'attr' => array(
                'class' => 'btn btn-success btn-block'
            )
        ));

        return $form;
    }

    /**
     * Displays a form to create a new FlashReport entity.
     *
     * @Route("/new", name="flashreport_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entity = new FlashReport();
        $idUsuario = $this->get('security.context')->getToken()->getUser()->getId();
        $usuario = $em->getRepository('PosBundle:Usuarios')->find($idUsuario);
        $entity->setIdUsuario($usuario);
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a FlashReport entity.
     *
     * @Route("/{id}", name="flashreport_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PosBundle:FlashReport')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find FlashReport entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing FlashReport entity.
     *
     * @Route("/{id}/edit", name="flashreport_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PosBundle:FlashReport')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find FlashReport entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a FlashReport entity.
    *
    * @param FlashReport $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(FlashReport $entity)
    {
        $form = $this->createForm(new FlashReportType(), $entity, array(
            'action' => $this->generateUrl('flashreport_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing FlashReport entity.
     *
     * @Route("/{id}", name="flashreport_update")
     * @Method("PUT")
     * @Template("PosBundle:FlashReport:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('PosBundle:FlashReport')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find FlashReport entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('flashreport_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a FlashReport entity.
     *
     * @Route("/{id}", name="flashreport_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('PosBundle:FlashReport')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find FlashReport entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('flashreport'));
    }

    /**
     * Creates a form to delete a FlashReport entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('flashreport_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
