<?php

namespace GS\pos\PosBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UsuarioMarca
 *
 * @ORM\Table(name="usuario_marca", indexes={@ORM\Index(name="fk_marca_idx", columns={"id_marca"}), @ORM\Index(name="fk_usuariomarca_usuario_idx", columns={"id_usuario"})})
 * @ORM\Entity
 */
class UsuarioMarca
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="nivel_acceso", type="integer", nullable=true)
     */
    private $nivelAcceso;

    /**
     * @var \Marca
     *
     * @ORM\ManyToOne(targetEntity="Marca")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_marca", referencedColumnName="id")
     * })
     */
    private $idMarca;

    /**
     * @var \Usuarios
     *
     * @ORM\ManyToOne(targetEntity="Usuarios")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_usuario", referencedColumnName="id")
     * })
     */
    private $idUsuario;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nivelAcceso
     *
     * @param integer $nivelAcceso
     * @return UsuarioMarca
     */
    public function setNivelAcceso($nivelAcceso)
    {
        $this->nivelAcceso = $nivelAcceso;

        return $this;
    }

    /**
     * Get nivelAcceso
     *
     * @return integer 
     */
    public function getNivelAcceso()
    {
        return $this->nivelAcceso;
    }

    /**
     * Set idMarca
     *
     * @param \GS\pos\PosBundle\Entity\Marca $idMarca
     * @return UsuarioMarca
     */
    public function setIdMarca(\GS\pos\PosBundle\Entity\Marca $idMarca = null)
    {
        $this->idMarca = $idMarca;

        return $this;
    }

    /**
     * Get idMarca
     *
     * @return \GS\pos\PosBundle\Entity\Marca 
     */
    public function getIdMarca()
    {
        return $this->idMarca;
    }

    /**
     * Set idUsuario
     *
     * @param \GS\pos\PosBundle\Entity\Usuarios $idUsuario
     * @return UsuarioMarca
     */
    public function setIdUsuario(\GS\pos\PosBundle\Entity\Usuarios $idUsuario = null)
    {
        $this->idUsuario = $idUsuario;

        return $this;
    }

    /**
     * Get idUsuario
     *
     * @return \GS\pos\PosBundle\Entity\Usuarios 
     */
    public function getIdUsuario()
    {
        return $this->idUsuario;
    }
}
