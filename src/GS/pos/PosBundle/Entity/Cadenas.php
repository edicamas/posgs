<?php

namespace GS\pos\PosBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cadenas
 *
 * @ORM\Table(name="cadenas", indexes={@ORM\Index(name="fk_cadenas_pais_idx", columns={"id_pais"})})
 * @ORM\Entity
 */
class Cadenas
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=75, nullable=true)
     */
    private $descripcion;

    /**
     * @var \Pais
     *
     * @ORM\ManyToOne(targetEntity="Pais")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_pais", referencedColumnName="id")
     * })
     */
    private $idPais;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Cadenas
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set idPais
     *
     * @param \GS\pos\PosBundle\Entity\Pais $idPais
     * @return Cadenas
     */
    public function setIdPais(\GS\pos\PosBundle\Entity\Pais $idPais = null)
    {
        $this->idPais = $idPais;

        return $this;
    }

    /**
     * Get idPais
     *
     * @return \GS\pos\PosBundle\Entity\Pais 
     */
    public function getIdPais()
    {
        return $this->idPais;
    }

    public function __toString()
    {

        return (string) $this->getDescripcion();
    }
}
