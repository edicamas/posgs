<?php

namespace GS\pos\PosBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Municipios
 *
 * @ORM\Table(name="municipios", indexes={@ORM\Index(name="fk_depto_idx", columns={"id_depto"})})
 * @ORM\Entity
 */
class Municipios
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=75, nullable=true)
     */
    private $descripcion;

    /**
     * @var \Deptos
     *
     * @ORM\ManyToOne(targetEntity="Deptos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_depto", referencedColumnName="id")
     * })
     */
    private $idDepto;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Municipios
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set idDepto
     *
     * @param \GS\pos\PosBundle\Entity\Deptos $idDepto
     * @return Municipios
     */
    public function setIdDepto(\GS\pos\PosBundle\Entity\Deptos $idDepto = null)
    {
        $this->idDepto = $idDepto;

        return $this;
    }

    /**
     * Get idDepto
     *
     * @return \GS\pos\PosBundle\Entity\Deptos 
     */
    public function getIdDepto()
    {
        return $this->idDepto;
    }

    public function __toString()
    {
        return (string) $this->getDescripcion();
    }
}
