<?php

namespace GS\pos\PosBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FlashReport
 *
 * @ORM\Table(name="flash_report", indexes={@ORM\Index(name="fk_tienda_idx", columns={"id_tienda"}), @ORM\Index(name="fk_marca_idx", columns={"id_marca"}), @ORM\Index(name="fk_usuario_idx", columns={"id_usuario"})})
 * @ORM\Entity
 */
class FlashReport
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="archivo_foto", type="string", length=100, nullable=true)
     */
    private $archivoFoto;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_reporte", type="date", nullable=true)
     */
    private $fechaReporte;

    /**
     * @var string
     *
     * @ORM\Column(name="observaciones", type="string", length=1000, nullable=true)
     */
    private $observaciones;

    /**
     * @var boolean
     *
     * @ORM\Column(name="logro", type="boolean", nullable=true)
     */
    private $logro;

    /**
     * @var string
     *
     * @ORM\Column(name="tipo_archivo", type="string", length=10, nullable=true)
     */
    private $tipoArchivo;

    /**
     * @var \Tiendas
     *
     * @ORM\ManyToOne(targetEntity="Tiendas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_tienda", referencedColumnName="id")
     * })
     */
    private $idTienda;

    /**
     * @var \Usuarios
     *
     * @ORM\ManyToOne(targetEntity="Usuarios")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_usuario", referencedColumnName="id")
     * })
     */
    private $idUsuario;

    /**
     * @var \Marca
     *
     * @ORM\ManyToOne(targetEntity="Marca")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_marca", referencedColumnName="id")
     * })
     */
    private $idMarca;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set archivoFoto
     *
     * @param string $archivoFoto
     * @return FlashReport
     */
    public function setArchivoFoto($archivoFoto)
    {
        $this->archivoFoto = $archivoFoto;

        return $this;
    }

    /**
     * Get archivoFoto
     *
     * @return string 
     */
    public function getArchivoFoto()
    {
        return $this->archivoFoto;
    }

    /**
     * Set fechaReporte
     *
     * @param \DateTime $fechaReporte
     * @return FlashReport
     */
    public function setFechaReporte($fechaReporte)
    {
        $this->fechaReporte = $fechaReporte;

        return $this;
    }

    /**
     * Get fechaReporte
     *
     * @return \DateTime 
     */
    public function getFechaReporte()
    {
        return $this->fechaReporte;
    }

    /**
     * Set observaciones
     *
     * @param string $observaciones
     * @return FlashReport
     */
    public function setObservaciones($observaciones)
    {
        $this->observaciones = $observaciones;

        return $this;
    }

    /**
     * Get observaciones
     *
     * @return string 
     */
    public function getObservaciones()
    {
        return $this->observaciones;
    }

    /**
     * Set logro
     *
     * @param boolean $logro
     * @return FlashReport
     */
    public function setLogro($logro)
    {
        $this->logro = $logro;

        return $this;
    }

    /**
     * Get logro
     *
     * @return boolean 
     */
    public function getLogro()
    {
        return $this->logro;
    }

    /**
     * Set tipoArchivo
     *
     * @param string $tipoArchivo
     * @return FlashReport
     */
    public function setTipoArchivo($tipoArchivo)
    {
        $this->tipoArchivo = $tipoArchivo;

        return $this;
    }

    /**
     * Get tipoArchivo
     *
     * @return string 
     */
    public function getTipoArchivo()
    {
        return $this->tipoArchivo;
    }

    /**
     * Set idTienda
     *
     * @param \GS\pos\PosBundle\Entity\Tiendas $idTienda
     * @return FlashReport
     */
    public function setIdTienda(\GS\pos\PosBundle\Entity\Tiendas $idTienda = null)
    {
        $this->idTienda = $idTienda;

        return $this;
    }

    /**
     * Get idTienda
     *
     * @return \GS\pos\PosBundle\Entity\Tiendas 
     */
    public function getIdTienda()
    {
        return $this->idTienda;
    }

    /**
     * Set idUsuario
     *
     * @param \GS\pos\PosBundle\Entity\Usuarios $idUsuario
     * @return FlashReport
     */
    public function setIdUsuario(\GS\pos\PosBundle\Entity\Usuarios $idUsuario = null)
    {
        $this->idUsuario = $idUsuario;

        return $this;
    }

    /**
     * Get idUsuario
     *
     * @return \GS\pos\PosBundle\Entity\Usuarios 
     */
    public function getIdUsuario()
    {
        return $this->idUsuario;
    }

    /**
     * Set idMarca
     *
     * @param \GS\pos\PosBundle\Entity\Marca $idMarca
     * @return FlashReport
     */
    public function setIdMarca(\GS\pos\PosBundle\Entity\Marca $idMarca = null)
    {
        $this->idMarca = $idMarca;

        return $this;
    }

    /**
     * Get idMarca
     *
     * @return \GS\pos\PosBundle\Entity\Marca 
     */
    public function getIdMarca()
    {
        return $this->idMarca;
    }
}
