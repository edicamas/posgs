<?php

namespace GS\pos\PosBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SupervisorTienda
 *
 * @ORM\Table(name="supervisor_tienda", indexes={@ORM\Index(name="fk_supervisortienda_usuarios_idx", columns={"id_usuario"}), @ORM\Index(name="fk_supervisortienda_tienda_idx", columns={"id_tienda"})})
 * @ORM\Entity
 */
class SupervisorTienda
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Usuarios
     *
     * @ORM\ManyToOne(targetEntity="Usuarios")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_usuario", referencedColumnName="id")
     * })
     */
    private $idUsuario;

    /**
     * @var \Tiendas
     *
     * @ORM\ManyToOne(targetEntity="Tiendas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_tienda", referencedColumnName="id")
     * })
     */
    private $idTienda;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idUsuario
     *
     * @param \GS\pos\PosBundle\Entity\Usuarios $idUsuario
     * @return SupervisorTienda
     */
    public function setIdUsuario(\GS\pos\PosBundle\Entity\Usuarios $idUsuario = null)
    {
        $this->idUsuario = $idUsuario;

        return $this;
    }

    /**
     * Get idUsuario
     *
     * @return \GS\pos\PosBundle\Entity\Usuarios 
     */
    public function getIdUsuario()
    {
        return $this->idUsuario;
    }

    /**
     * Set idTienda
     *
     * @param \GS\pos\PosBundle\Entity\Tiendas $idTienda
     * @return SupervisorTienda
     */
    public function setIdTienda(\GS\pos\PosBundle\Entity\Tiendas $idTienda = null)
    {
        $this->idTienda = $idTienda;

        return $this;
    }

    /**
     * Get idTienda
     *
     * @return \GS\pos\PosBundle\Entity\Tiendas 
     */
    public function getIdTienda()
    {
        return $this->idTienda;
    }
}
