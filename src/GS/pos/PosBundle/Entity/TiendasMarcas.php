<?php

namespace GS\pos\PosBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TiendasMarcas
 *
 * @ORM\Table(name="tiendas_marcas", indexes={@ORM\Index(name="fk_tiendasmarcas_tiendas_idx", columns={"id_tienda"}), @ORM\Index(name="fk_tiendasmarcas_marcas_idx", columns={"id_marca"})})
 * @ORM\Entity
 */
class TiendasMarcas
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Tiendas
     *
     * @ORM\ManyToOne(targetEntity="Tiendas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_tienda", referencedColumnName="id")
     * })
     */
    private $idTienda;

    /**
     * @var \Marca
     *
     * @ORM\ManyToOne(targetEntity="Marca")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_marca", referencedColumnName="id")
     * })
     */
    private $idMarca;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idTienda
     *
     * @param \GS\pos\PosBundle\Entity\Tiendas $idTienda
     * @return TiendasMarcas
     */
    public function setIdTienda(\GS\pos\PosBundle\Entity\Tiendas $idTienda = null)
    {
        $this->idTienda = $idTienda;

        return $this;
    }

    /**
     * Get idTienda
     *
     * @return \GS\pos\PosBundle\Entity\Tiendas 
     */
    public function getIdTienda()
    {
        return $this->idTienda;
    }

    /**
     * Set idMarca
     *
     * @param \GS\pos\PosBundle\Entity\Marca $idMarca
     * @return TiendasMarcas
     */
    public function setIdMarca(\GS\pos\PosBundle\Entity\Marca $idMarca = null)
    {
        $this->idMarca = $idMarca;

        return $this;
    }

    /**
     * Get idMarca
     *
     * @return \GS\pos\PosBundle\Entity\Marca 
     */
    public function getIdMarca()
    {
        return $this->idMarca;
    }
}
