<?php

namespace GS\pos\PosBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tiendas
 *
 * @ORM\Table(name="tiendas", indexes={@ORM\Index(name="fk_cadena_idx", columns={"id_cadena"}), @ORM\Index(name="fk_depto_idx", columns={"id_depto"}), @ORM\Index(name="fk_muni_tienda_idx", columns={"id_muni"})})
 * @ORM\Entity
 */
class Tiendas
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre_tda", type="string", length=45, nullable=true)
     */
    private $nombreTda;

    /**
     * @var string
     *
     * @ORM\Column(name="direccion_tda", type="string", length=45, nullable=true)
     */
    private $direccionTda;

    /**
     * @var \Municipios
     *
     * @ORM\ManyToOne(targetEntity="Municipios")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_muni", referencedColumnName="id")
     * })
     */
    private $idMuni;

    /**
     * @var \Cadenas
     *
     * @ORM\ManyToOne(targetEntity="Cadenas")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_cadena", referencedColumnName="id")
     * })
     */
    private $idCadena;

    /**
     * @var \Deptos
     *
     * @ORM\ManyToOne(targetEntity="Deptos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_depto", referencedColumnName="id")
     * })
     */
    private $idDepto;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombreTda
     *
     * @param string $nombreTda
     * @return Tiendas
     */
    public function setNombreTda($nombreTda)
    {
        $this->nombreTda = $nombreTda;

        return $this;
    }

    /**
     * Get nombreTda
     *
     * @return string 
     */
    public function getNombreTda()
    {
        return $this->nombreTda;
    }

    /**
     * Set direccionTda
     *
     * @param string $direccionTda
     * @return Tiendas
     */
    public function setDireccionTda($direccionTda)
    {
        $this->direccionTda = $direccionTda;

        return $this;
    }

    /**
     * Get direccionTda
     *
     * @return string 
     */
    public function getDireccionTda()
    {
        return $this->direccionTda;
    }

    /**
     * Set idMuni
     *
     * @param \GS\pos\PosBundle\Entity\Municipios $idMuni
     * @return Tiendas
     */
    public function setIdMuni(\GS\pos\PosBundle\Entity\Municipios $idMuni = null)
    {
        $this->idMuni = $idMuni;

        return $this;
    }

    /**
     * Get idMuni
     *
     * @return \GS\pos\PosBundle\Entity\Municipios 
     */
    public function getIdMuni()
    {
        return $this->idMuni;
    }

    /**
     * Set idCadena
     *
     * @param \GS\pos\PosBundle\Entity\Cadenas $idCadena
     * @return Tiendas
     */
    public function setIdCadena(\GS\pos\PosBundle\Entity\Cadenas $idCadena = null)
    {
        $this->idCadena = $idCadena;

        return $this;
    }

    /**
     * Get idCadena
     *
     * @return \GS\pos\PosBundle\Entity\Cadenas 
     */
    public function getIdCadena()
    {
        return $this->idCadena;
    }

    /**
     * Set idDepto
     *
     * @param \GS\pos\PosBundle\Entity\Deptos $idDepto
     * @return Tiendas
     */
    public function setIdDepto(\GS\pos\PosBundle\Entity\Deptos $idDepto = null)
    {
        $this->idDepto = $idDepto;

        return $this;
    }

    /**
     * Get idDepto
     *
     * @return \GS\pos\PosBundle\Entity\Deptos 
     */
    public function getIdDepto()
    {
        return $this->idDepto;
    }

    public function __toString()
    {
        return (string) $this->getNombreTda();
    }
}
