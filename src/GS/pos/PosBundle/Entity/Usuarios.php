<?php

namespace GS\pos\PosBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;


/**
 * Usuarios
 *
 * @ORM\Table(name="usuarios", indexes={@ORM\Index(name="fk_usuarios_pais_idx", columns={"id_pais"})})
 * @ORM\Entity
 */
class Usuarios implements UserInterface, \Serializable
{
     public function getUserName()
    {
        return $this->getUsuario();
    }
    public function getPassword()
    {
        return $this->getContrasenia();
    }
    public function getSalt()
    {
        return '';
    }
    public function getRoles()
    {
        return array($this->getRole());
    }
     public function eraseCredentials()
    {
    }
         public function serialize()
         {
                return serialize($this->id);
         }

         public function unserialize($data)
         {
                $this->id = unserialize($data);
         }
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="usuario", type="string", length=45, nullable=false)
     */
    private $usuario;

    /**
     * @var string
     *
     * @ORM\Column(name="contrasenia", type="string", length=255, nullable=false)
     */
    private $contrasenia;

    /**
     * @var string
     *
     * @ORM\Column(name="role", type="string", length=45, nullable=false)
     */
    private $role;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=100, nullable=true)
     */
    private $descripcion;

    /**
     * @var string
     *
     * @ORM\Column(name="correo", type="string", length=100, nullable=true)
     */
    private $correo;

    /**
     * @var integer
     *
     * @ORM\Column(name="conectado", type="integer", nullable=true)
     */
    private $conectado;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_sesion", type="date", nullable=true)
     */
    private $fechaSesion;

    /**
     * @var \Pais
     *
     * @ORM\ManyToOne(targetEntity="Pais")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_pais", referencedColumnName="id")
     * })
     */
    private $idPais;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set usuario
     *
     * @param string $usuario
     * @return Usuarios
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return string 
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Set contrasenia
     *
     * @param string $contrasenia
     * @return Usuarios
     */
    public function setContrasenia($contrasenia)
    {
        $this->contrasenia = $contrasenia;

        return $this;
    }

    /**
     * Get contrasenia
     *
     * @return string 
     */
    public function getContrasenia()
    {
        return $this->contrasenia;
    }

    /**
     * Set role
     *
     * @param string $role
     * @return Usuarios
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return string 
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Usuarios
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set correo
     *
     * @param string $correo
     * @return Usuarios
     */
    public function setCorreo($correo)
    {
        $this->correo = $correo;

        return $this;
    }

    /**
     * Get correo
     *
     * @return string 
     */
    public function getCorreo()
    {
        return $this->correo;
    }

    /**
     * Set conectado
     *
     * @param integer $conectado
     * @return Usuarios
     */
    public function setConectado($conectado)
    {
        $this->conectado = $conectado;

        return $this;
    }

    /**
     * Get conectado
     *
     * @return integer 
     */
    public function getConectado()
    {
        return $this->conectado;
    }

    /**
     * Set fechaSesion
     *
     * @param \DateTime $fechaSesion
     * @return Usuarios
     */
    public function setFechaSesion($fechaSesion)
    {
        $this->fechaSesion = $fechaSesion;

        return $this;
    }

    /**
     * Get fechaSesion
     *
     * @return \DateTime 
     */
    public function getFechaSesion()
    {
        return $this->fechaSesion;
    }

    /**
     * Set idPais
     *
     * @param \GS\pos\PosBundle\Entity\Pais $idPais
     * @return Usuarios
     */
    public function setIdPais(\GS\pos\PosBundle\Entity\Pais $idPais = null)
    {
        $this->idPais = $idPais;

        return $this;
    }

    /**
     * Get idPais
     *
     * @return \GS\pos\PosBundle\Entity\Pais 
     */
    public function getIdPais()
    {
        return $this->idPais;
    }
    public function __toString(){
        return $this->getUsuario();
    }
}
