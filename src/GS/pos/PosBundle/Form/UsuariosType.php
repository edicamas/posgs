<?php

namespace GS\pos\PosBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UsuariosType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('usuario',null,array(
                'label'=>'Usuario:',
                'attr'=>array(
                            'class'=>'form-control'
                            )
            ))
            ->add('contrasenia',null,array(
                'label'=>'Contraseña:',
                'attr'=>array(
                            'class'=>'form-control'
                                )
            ))
            ->add('role','choice', array(
            'label'     =>  'Tipo de Usuario:',
            'choices'   =>  array(
                            'SA'=>'Super Admin',
                            'A'=>'Administrador',
                            'S'=>'Supervisor',
                            'C'=>'Cliente',
                            'U'=>'User'
                                ),
            'attr'      =>  array(
                            'class'=>'form-control'
                                )
                ))
            ->add('descripcion',null,array(
                'label'=>'Nombre Usuario:',
                'attr'=>array(
                            'class'=>'form-control'
                            )
            ))
            ->add('correo',null,array(
                'label'=>'Correo:',
                'attr'=>array(
                            'class'=>'form-control'
                            )
            ))
            ->add('conectado',null,array(
                'label'=>'',
                'attr'=>array(
                            'class'=>'form-control'
                            )
            ))
            ->add('fechaSesion',null,array(
                'label'=>'',
                'attr'=>array(
                            'class'=>'form-control'
                            )
            ))
            ->add('idPais',null,array(
                'label'=>'Pais:',
                'attr'=>array(
                            'class'=>'form-control'
                            )
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'GS\pos\PosBundle\Entity\Usuarios'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'gs_pos_posbundle_usuarios';
    }
}
