<?php

namespace GS\pos\PosBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class MarcaType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('descripcion',null,array(
                  'label' => 'Descripcion:',
                  'attr'  => array(
                                'class' => 'form-control'
                                )
                  ))
            ->add('idPais',null,array(
                  'label'   => 'Pais:',
                  'attr'    => array(
                                'class' => 'form-control'
                            )
                ))
            ->add('idUsuario',null,array(
                  'label'   => '',
                  'attr'    => array(
                               'class' => 'form-control'
                            )
                ));
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'GS\pos\PosBundle\Entity\Marca'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'gs_pos_posbundle_marca';
    }
}
