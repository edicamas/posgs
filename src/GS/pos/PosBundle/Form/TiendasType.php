<?php

namespace GS\pos\PosBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TiendasType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombreTda',null,array(
                'label' => 'Nombre de Tienda',
                'attr'  => array(
                    'class' => 'form-control'
                )
            ))
            ->add('direccionTda',null,array(
                'label' => 'Dirección',
                'attr'  => array(
                    'class' => 'form-control'
                )
            ))
            ->add('idMuni',null,array(
                'label' => 'Municipio',
                'attr'  => array(
                    'class' => 'form-control'
                )
            ))
            ->add('idCadena',null,array(
                'label' => 'Cadena',
                'attr'  => array(
                    'class' => 'form-control'
                )
            ))
            ->add('idDepto',null,array(
                'label' => 'Departamento',
                'attr'  => array(
                    'class' => 'form-control'
                )
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'GS\pos\PosBundle\Entity\Tiendas'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'gs_pos_posbundle_tiendas';
    }
}
