<?php

namespace GS\pos\PosBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class FlashReportType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('archivoFoto','file',array(
                'label' => '',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('fechaReporte',null,array(
                'label' => 'Fecha de Reporte',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('observaciones','textarea',array(
                'label' => 'Observaciones',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('logro',null,array(
                'label' => 'Es Logro'
            ))
            ->add('tipoArchivo',null,array(
                'label' => '',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('idTienda',null,array(
                'label' => 'Tienda',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('idUsuario',null,array(
                'label' => 'Usuario',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('idMarca',null,array(
                'label' => 'Marca',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('idCadena','entity',array(
                'class' => 'PosBundle:Cadenas',
                'mapped' => false,
                'attr' => array(
                    'class' => 'form-control'
                ),
                'label' => 'Cadena'
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'GS\pos\PosBundle\Entity\FlashReport'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'gs_pos_posbundle_flashreport';
    }
}
